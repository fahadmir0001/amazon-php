<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Amazon extends CI_Controller{
    public function view($page = "amazon") {
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
            show_404();
        }
        $this->load->view('pages/'.$page);
    }

    public function product_advertising_API(){
        // Your Access Key ID, as taken from the Your Account page
        $access_key_id = "AKIAI3ID2L665O7ZL3LA";

        // Your Secret Key corresponding to the above ID, as taken from the Your Account page
        $secret_key = "xbA0Gd+QjQY8Pe+HhpVnWkzKktexCDZHmWS8g7eE";

        // The region you are interested in
        $endpoint = "webservices.amazon.com";

        $uri = "/onca/xml";

        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemLookup",
            "AWSAccessKeyId" => "AKIAI3ID2L665O7ZL3LA",
            "AssociateTag" => "hanzous-20",
            "ItemId" => $this->input->post('dat'),
            "IdType" => "ASIN",
            "ResponseGroup" => "ItemAttributes,ItemIds"
        );

        // Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        // Sort the parameters by key
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }

        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);

        // Generate the string to be signed
        $string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $secret_key, true));

        // Generate the signed URL
        $request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

        $xml = simplexml_load_string(file_get_contents($request_url));
        $json = json_encode($xml);
        echo $json;
    }


    public function keyword_search() {
        $product_title = $this->input->post('title');
        $words = explode(' ', preg_replace('/[0-9]+/', '', $product_title));
        header('Content-Type: application/json');
        echo json_encode($words);
    }
}
