<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to Amazon Product Search</title>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <img style="margin:10% 0 5% 33%" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Amazon_logo_plain.svg/2000px-Amazon_logo_plain.svg.png" width="40%" height="40%"/>
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4">
            <input class="form-control" id="prod-id" placeholder="Enter Product Id here"/>
        </div>
        <div class="col-sm-2 col-md-2">
            <button class="btn btn-primary" id="search">Search</button>
        </div>
    </div>
    <div class="row">
        <div style="margin-top: 5%" class="col-sm-offset-4 col-sm-4 col-md-offset-2 col-md-8">
            <div id="error-state" style="display: none" class="col-md-offset-4 col-md-4 alert alert-danger">Product Not Found</div>
            <table class="table" id="keyword-table">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

</body>

<script>
    $(document).ready(function() {
        $("#search").click(function () {

            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>index.php/amazon/product_advertising_API",
                data: {dat: $('#prod-id').val()},
                cache: false,
                success:
                    function (data) {
                        TitleTransformation(data);
                    }
            });

            function TitleTransformation(data) {
                var Item=JSON.parse(data).Items.Item;

                var Title = Item && Item.ItemAttributes.Title;

                if(Title) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url() ?>index.php/amazon/keyword_search",
                        data: {title: Title},
                        cache: false,
                        success: function (data) {
                            KeywordEndPoint(data)
                        }
                    });
                }
                else{
                    $("#error-state").css("display","block");
                }
            }

            function KeywordEndPoint(data) {
                var endpoint = "http://completion.amazon.com/search/complete?search-alias=aps&client=amazon-search-ui&mkt=1&q=";
                $("#keyword-table thead").append('<tr><th>Title</th><th class="text-center">Keywords</th></tr>');

                $.each(data, function (index, queries) {
                    $.ajax({
                        type: "GET",
                        url: endpoint + queries,
                        header: 'Access-Control-Allow-Origin: *',
                        dataType: 'jsonp',
                        cache: false,
                        success: function(data) {
                            DataInsertion(queries, data);
                        }
                    });
                });
            }

            function DataInsertion(title, data) {
                $.each(data, (key, value) => {
                    key === 1 && $("#keyword-table tbody").append('<tr><td>' + title + '</td><td>' + value + '</td></tr>');
                });
            }
        });
        $("#prod-id").click(function(){
            $("#error-state").css("display","none");
        });
    });

</script>
</html>